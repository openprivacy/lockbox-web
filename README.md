![](./img/logo.png)

This is the PHP/webserver component for Lockbox. For instructions visit [the Lockbox app's main repository](https://git.openprivacy.ca/openprivacy/lockbox).


### Acknowledgements

* thanks to Steve Weis (@sweis) for reporting a potential timing attack against admin password validation