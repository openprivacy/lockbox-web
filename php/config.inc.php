<?php

// This is the submissions file -- it should not be in your web directory!!!
define('FILE', '/var/www/data/lockbox.dat');

// Page/form title
define('MSG_TITLE', 'Lockbox by Open Privacy');

// Message given to successful submissions
define('MSG_SUCCESS', '
<h1>Submission Received</h1>
<p>Thank you for your submission to '.MSG_TITLE.'!</p>
');

// Message for when a submission error occurred
define('MSG_FAIL', '
<h1>Error receiving submission</h1>\n";
<p>There was a problem with your submission.  Please hit the Back button on your browser and try again in a few minutes, or contact the form\'s administrator if the problem persists.</p>
	');

// Displayed at the bottom of every page
define('MSG_FOOTER', '&copy;2020 <a href="https://openprivacy.ca/" target="_blank">Our Awesome Org</a> - All rights reserved. Form powered by <a href="https://git.openprivacy.ca/openprivacy/lockbox" target="_blank">Lockbox</a> by <a href="https://openprivacy.ca" target="_blank">Open Privacy</a>');

// restrict the (optional) admin form to these IPs
$ADMIN_IPS = array(
/*	"1.2.3.501", // erinn
        "9.5.1.06", // sarah
        "92.102.94.l" // dan*/
);

// if you would like you disable the IP check (not recommended!) you can use this line instead:
// $ADMIN_IPS = array();

// password for downloading submissions from the admin form
define('DL_PASS', "");


// ============ include file locations -- you shouldn't need to change below this line

define('header', 'php/header.tpl.php');
define('footer', 'php/footer.tpl.php');
define('form', 'php/form.inc.php');

?>
