<?php

require_once 'php/config.inc.php';

// To generate a password hash, use the following command on a secure local machine:
// php -r 'echo password_hash("adminpasswordhere", PASSWORD_DEFAULT);'
$password = '$2y$10$ORfmg3iGr25X2Y.MYxTp5OxYC02dUF8swQ/dbeYreMR0ea0LIUk0u';

define('FORMCONTENTS', '<form method="post">
		<p>Password: <input type="password" name="password"> <input type="submit" class="button-primary" value="Download encrypted submissions"></p>
		</form>');

if (strlen(DL_PASS) < 1) {
	include header;
	echo "<p>Admin password cannot be blank. Update <tt>DL_PASS</tt> in <tt>php/config.inc.php</tt> to use this feature.</p>";
	include footer;
	exit();
}

if (strpos(__FILE__, 'admin.php') !== false) {
	include header;
	echo "<p>Please rename <tt>admin.php</tt> before using this tool. We suggest something like: <tt>".hash("sha256", microtime() . random_int(PHP_INT_MIN, PHP_INT_MAX) . "t08ujli4helfiubhi8uy98yhe4f@#RETHrtg80uj").".php</tt></p>";
	include footer;
	exit();
}

$ip = $_SERVER['REMOTE_ADDR'];
if (count($ADMIN_IPS) > 0 && array_search($ip, $ADMIN_IPS) === false) {
	include header;
	echo "<p>Sorry, but your IP address does not have permission to access this tool. Please contact the form maintainer to resolve this.</p>";
	echo "<!-- $ip -->";
	include footer;
} else {
	if (isset($_POST) && isset($_POST['password'])) {
		if (password_verify($_POST['password'], $password)) {
			header('Content-Type: application/octet-stream');
			header("Content-Transfer-Encoding: Binary"); 
			header("Content-disposition: attachment; filename=\"submissions.dat\""); 
			readfile(FILE);
			exit();
		} else {
			include header;
			echo "<p>Wrong password, sorry.\n";
			echo form;
			include footer;
		}
	} else {
		include header;
		echo FORMCONTENTS;
		date_default_timezone_set('America/Vancouver');
		echo "<p>Last submission received at " . date ("F d Y H:i:s", filemtime(FILE)) . ".</p>\n";
		include footer;
	}
}

?>
